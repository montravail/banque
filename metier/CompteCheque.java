package metier;

/**
 * La classe CompteChèque représente un compte chèque (non rémunéré).
 *
 * @author V. Britelle
 * @version 23-oct-2020
 */
public class CompteCheque extends Compte
{

    /**
     * Crée un nouveau compte chèque au solde nul.
     */
    public CompteCheque(String numero, Banque banque, Client client)
    {
        super(numero, banque, client);
    }

    /**
     * Créei un nouveau compte chèque avec un solde initial.
     */
    public CompteCheque(String numero, Banque banque, Client client, double solde)
    {
        super(numero, banque, client, solde);
    }
}
