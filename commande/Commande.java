package commande;
/**
 * La classe Commande représente un commande exécutable
 * que l'on peut associer à un item de menu par exemple.
 *
 * @author  Vincent
 */
public abstract class Commande
{
    // Retourne false signifiera quon veut quitter l'application
    public abstract void execute();
}
